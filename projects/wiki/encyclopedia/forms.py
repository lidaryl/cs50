from django import forms


class SearchForm(forms.Form):
    query = forms.CharField(label="",
                            help_text="",
                            max_length=50,
                            widget=forms.TextInput(
                                attrs={'class': 'search',
                                       'placeholder': 'Search Encyclopedia'}
                                ))