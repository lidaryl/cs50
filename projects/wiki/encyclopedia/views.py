from django.shortcuts import render, redirect
from django.http import Http404, HttpResponseRedirect

from . import util
from . import forms


def index(request):
    return render(request, "encyclopedia/index.html", {
        "searchform": forms.SearchForm(),
        "entries": util.list_entries()
    })

def wiki_render(request, title, entry_content):
    return render(request, "encyclopedia/wiki.html", {
                "title": title,
                "entry_content": entry_content,
                "searchform": forms.SearchForm(),
            })

def wiki(request, title):
    entry_content = util.get_entry(title)
    
    if entry_content:
        return wiki_render(request, title, entry_content)
    else:
        raise Http404("Entry not found.")

"""
Search Handling
"""

def search(request):
    if request.method == 'POST':
        form = forms.SearchForm(request.POST)
        
        if form.is_valid():
            query = form.cleaned_data['query']
            entry_content = util.get_entry(query)

            if entry_content:
                return wiki_render(request, query, entry_content)
            else:
                search_results = util.search_entries(query)

                return render(request, "encyclopedia/results.html", {
                    "search_results": search_results,
                    "searchform": forms.SearchForm(),
                })
    else:
        return redirect('index')
