document.addEventListener('DOMContentLoaded', () => {
    const search_text = document.querySelector('.search')

    search_text.onKeyUp = function(e) {
        if (e.key === 'Enter' || e.keyCode === 13) {
            if (search_text.value > 0) {
                document.querySelector('#search_form').submit()
            }
        }
    }
})